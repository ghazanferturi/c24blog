<?php
$servername = "localhost";
$username = "homestead";
$password = "secret";
$dbname = "c24blogDB";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// sql to create table
$sql = "CREATE TABLE Article (
		id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
		title VARCHAR(60) NOT NULL,
		date DATE,
		time TIMESTAMP,
		picture_link VARCHAR(100),
		content MEDIUMTEXT, 
		author_id INT(11)
		
	)";

if ($conn->query($sql) === TRUE) {
    echo "Table Author created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}

$conn->close();
?>
