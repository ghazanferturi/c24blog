<?php
$servername = "localhost";
$username = "homestead";
$password = "secret";
$dbname = "c24blogDB";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// sql to create table
$sql = "CREATE TABLE User (
		id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
		username VARCHAR(60) NOT NULL,
		password VARCHAR(255) NOT NULL,
		email VARCHAR(100) NOT NULL,
		author_id INT(11),
		commentor_id INT(11)
		
	)";

if ($conn->query($sql) === TRUE) {
    echo "Table User created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}

$conn->close();
?>
