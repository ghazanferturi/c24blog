<?php namespace App\Controller;

require __DIR__ . '/../../vendor/autoload.php';

class Home
{
    public function indexAction()
    { 
        // you could add the twig package 'composer require "twig/twig:^2.0"' 
        // and use it as "echo $twig->render('index', ['name' => 'Fabien']);"
	echo 'Hello World TEST' . PHP_EOL;

	$loader = new \Twig\Loader\ArrayLoader([
    			'index' => 'Hello {{ name }}!',
		]);
	$twig = new \Twig\Environment($loader);

	echo $twig->render('index', ['name' => 'Fabien']);

    }

    public function createBasicLayout()
    {
	
    }

    public function login()
    {
	
    }
}
