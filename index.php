<?php
require __DIR__ . '/vendor/autoload.php';

use App\Lib\Config;
//use App\Lib\Logger;
use App\Lib\App;
use App\Lib\Router;
use App\Lib\Request;
use App\Lib\Response;
use App\Controller\Home;

//echo $_SERVER['REQUEST_METHOD'];

//$LOG_PATH = Config::get('LOG_PATH', '');
//echo "[LOG_PATH]: $LOG_PATH";

//Logger::enableSystemLogs();
//$logger = Logger::getInstance();
//$logger->info('Hello World');


Router::get('/', function () {
    //echo 'Hello World';
    (new Home())->indexAction();
});

Router::get('/post/([0-9]*)', function (Request $req, Response $res) {
    $res->toJSON([
        'post' =>  ['id' => $req->params[0]],
        'status' => 'ok'
    ]);
});


App::run();
